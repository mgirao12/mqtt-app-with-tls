package com.example.MQTTAppTLS;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;


import java.nio.charset.StandardCharsets;
import java.util.Timer;
import java.util.TimerTask;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import mqttapptls.R;


//import mqttapptls.R;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    public static final String EXTRA_MESSAGE = "hola";
    private EditText mMessageEditText;
    public static final int TEXT_REQUEST = 1;
    private TextView mReplyHeadTextView;
    private TextView mReplyTextView;
    public MqttConnectOptions options;
    public MqttAndroidClient mClient;
    public String textoSuscripcion;
    public Handler HandlerPP;
    public TextView mSuscribirseTextView;

    public String serverURI = "ssl://broker.emqx.io:8883";
    public String clientId = "Publicador";

    @Override

    public void onSaveInstanceState(@NonNull Bundle outState) { //Funcion para guardar si se onDestroy la actividad!! Lo guarda cuando onDestroy o onStop
        super.onSaveInstanceState(outState);
        if (mReplyHeadTextView.getVisibility() == View.VISIBLE) {
            outState.putBoolean("reply_visible", true); //Lo que queremos guardar
            outState.putString("reply_text",mReplyTextView.getText().toString()); //Lo mismo, los valores importantes que perdemos
        }

        Log.e(LOG_TAG, "-------");
        Log.e(LOG_TAG, "ESTOYYYYYYY");
    }


    public void onStart(){
        super.onStart();
        Log.d(LOG_TAG, "onStart");
    }

    public void onPause(){
        super.onPause();
        Log.d(LOG_TAG, "onPause");
    }

    public void onRestart(){
        super.onRestart();
        Log.d(LOG_TAG, "onRestart");
    }

    public void onResume(){
        super.onResume();
        Log.d(LOG_TAG, "onResume");
    }

    public void onStop(){
        super.onStop();
        Log.d(LOG_TAG, "onStop");
    }

    public void onDestroy(){
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }

    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode, resultCode, data);
        if (requestCode == TEXT_REQUEST) {
            if (resultCode == RESULT_OK) {
                String reply = data.getStringExtra(SecondActivity.EXTRA_REPLY);
                mReplyHeadTextView.setVisibility(View.VISIBLE);
                mReplyTextView.setText(reply);
                mReplyTextView.setVisibility(View.VISIBLE);
            }
        }

    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //Define qué diseño quieres usar!!
        boolean isVisible = false; //Lo declaramos aqui sino da problemas con el if
        // Initialize all the view variables
        mMessageEditText = (EditText) findViewById(R.id.editText_main);
        mReplyHeadTextView = findViewById(R.id.text_header_reply);
        mReplyTextView = findViewById(R.id.text_message_reply);
        mSuscribirseTextView = findViewById(R.id.suscribirseView);

        // Restore the state.
        if (savedInstanceState != null) {
            isVisible = savedInstanceState.getBoolean("reply_visible");
        }


        if (isVisible) {
            mReplyHeadTextView.setVisibility(View.VISIBLE);
            mReplyTextView.setText(savedInstanceState.getString("reply_text"));
            mReplyTextView.setVisibility(View.VISIBLE);
        }

        Log.d(LOG_TAG, "-------");
        Log.d(LOG_TAG, "onCreate");


        mqttsetup(this);
        //initClient();
        MqttConnect();

        mClient.setCallback(new MqttCallbackExtended() {

            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                Log.e(LOG_TAG, "Conectado primero!!!");
                showToast("Conectado primero");
            }

            @Override
            public void connectionLost(Throwable cause) {
                Log.e(LOG_TAG, "Error, no se ha conectado primero");
                showToast("Desconectado primero");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) {
                /*textoSuscripcion = String.valueOf(message.getPayload());*/
                textoSuscripcion = new String (message.getPayload(), StandardCharsets.UTF_8);
                //Log.e(LOG_TAG, textoSuscripcion);
                //showToast(textoSuscripcion);
                //showToast("Recibidisisisisisimo");

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });


        HandlerPP = new Handler (Looper.getMainLooper());
        Timer timerSuscriptor = new Timer ();
        timerSuscriptor.schedule(new TimerTask() {
            @Override
            public void run() {
                ActualizaSuscriptor();
            }
        }, 0, 5000);
    }

    private void mqttsetup (Context context){

        mClient = new MqttAndroidClient(getApplicationContext(), serverURI, clientId);

        options = new MqttConnectOptions();
        options.setCleanSession(true);
        options.setUserName("username");
        options.setPassword("password".toCharArray());
        options.setAutomaticReconnect(true);

        if (serverURI.contains("ssl")) {
            SocketFactory.SocketFactoryOptions socketFactoryOptions = new SocketFactory.SocketFactoryOptions();
            try {
                socketFactoryOptions.withCaInputStream(context.getResources().openRawResource(R.raw.broker_emqx_io_ca));
                options.setSocketFactory(new SocketFactory(socketFactoryOptions));
            } catch (IOException | NoSuchAlgorithmException | KeyStoreException | CertificateException | KeyManagementException | UnrecoverableKeyException e) {
                e.printStackTrace();
            }
        }


    }

    public void launchSecondActivity(View view) {
        Log.d(LOG_TAG, "Button clicked!!");
        /*Intent intent = new Intent(this, SecondActivity.class);*/
        /*String message = mMessageEditText.getText().toString();*/
        /*intent.putExtra(EXTRA_MESSAGE, message);
        startActivityForResult(intent, TEXT_REQUEST);*/ //Esta obsoleto wtf
    }


    private void initClient() throws MqttException {


        mClient.setCallback(new MqttCallbackExtended() {

            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                Log.e(LOG_TAG, "Conectado primero!!!");
                showToast("Conectado primero");
            }

            @Override
            public void connectionLost(Throwable cause) {
                Log.e(LOG_TAG, "Error, no se ha conectado primero");
                showToast("Desconectado primero");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) {
                /*textoSuscripcion = String.valueOf(message.getPayload());*/
                textoSuscripcion = new String (message.getPayload(), StandardCharsets.UTF_8);
                //Log.e(LOG_TAG, textoSuscripcion);
                //showToast(textoSuscripcion);
                //showToast("Recibidisisisisisimo");

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });

        connect(this.options, this.mClient);
    }


    private void connect(MqttConnectOptions options, MqttAndroidClient mClient) throws MqttException {
        mClient.connect(options, new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken asyncActionToken) {
                Log.e(LOG_TAG, "Conectado segundo!!!");
                showToast("Conectado segundo");
            }

            @Override
            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                Log.e(LOG_TAG, "Error, no se ha conectado segundo");
                showToast("Desconectado segundo");
            }
        });
    }

    void MqttConnect() {
        try {

            final IMqttToken token = mClient.connect(options);
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    Log.e("MQTT:", "connected, token:" + asyncActionToken.toString());
                    //subscribe(TOPIC, (byte) 1);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    Log.e("MQTT:", "not connected" + asyncActionToken.toString());
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }


    private void publish(@NonNull MqttAndroidClient mClient) throws MqttException {
        String message = mMessageEditText.getText().toString();
        mClient.publish("prueba", message.getBytes(), 0, false, null, new IMqttActionListener() {
            @Override
            public void onSuccess(IMqttToken asyncActionToken) {
                Log.e(LOG_TAG, "Publicado!!!");
                //showToast("Publicado!!!");
            }

            @Override
            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                Log.e(LOG_TAG, "ERROR!! No se ha publicado");
                showToast("ERROR!! No se ha publicado!!");
            }
        });
    }

    /*public void publicar(View view, MqttAndroidClient mClient) throws MqttException {
        publish(this.mClient);
    }*/

    public void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void publicar(View view) throws MqttException {
        publish(this.mClient);
    }


    private void subscribe() throws MqttException {
        this.mClient.subscribe("prueba", 0, null, new IMqttActionListener() { //Usamos mismo client para suscribirnos
            @Override
            public void onSuccess(IMqttToken asyncActionToken) {
                Log.e(LOG_TAG, "Suscrito!!!");
                showToast("Suscrito!!!");
            }

            @Override
            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                Log.e(LOG_TAG, "NO Suscrito!!!");
                showToast("NO Suscrito!!!");
            }
        });
    }

    public void suscribir(View view) throws MqttException { //Boton de suscripcion
        subscribe();
    }

    public void ActualizaSuscriptor() {
        HandlerPP.post(new Runnable() {
            @Override
            public void run() {
                mSuscribirseTextView.setText(textoSuscripcion);
            }
        });
    }
}